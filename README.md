# Microservices with Docker, Flask, and React

[![Build Status](https://travis-ci.com/AntoineTorrini/testdriven-app.svg?branch=master)](https://travis-ci.com/AntoineTorrini/testdriven-app)

## Useful commands

Restart the container:
```bash
docker-compose up -d
```

Rebuild the container:
```bash
docker-compose up -d --build
```

Run the tests for users:
```bash
docker-compose exec users python manage.py test
```